{
  description = "Application packaged using poetry2nix";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.poetry2nix-src.url = "github:nix-community/poetry2nix";

  outputs = { self, nixpkgs, flake-utils, poetry2nix-src }:
  flake-utils.lib.eachDefaultSystem (system: let
    pkgs = import nixpkgs {
      inherit system;
      overlays = [ poetry2nix-src.overlay ];
    };

    poetry2nix = pkgs.poetry2nix;

    env = poetry2nix.mkPoetryEnv {
      projectDir = ./.;
      preferWheels = true;

      editablePackageSources = {
        test_mpc = ./.;
      };

      overrides = [
        poetry2nix.defaultPoetryOverrides (self: super: {
          casadi = super.casadi.overridePythonAttrs(old: {
            autoPatchelfIgnoreMissingDeps = true;
          });
        })
      ];
    };

    #env = app.dependencyEnv;
  in rec {

    devShell = pkgs.mkShell {
      buildInputs = with pkgs; [
        poetry
        env
      ];
    };
  });
}
