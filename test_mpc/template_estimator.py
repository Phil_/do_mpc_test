import do_mpc

def get_estimator(model):
    mhe = do_mpc.estimator.MHE(model)

    mhe.setup()

    # Set parameters:
    setup_mhe = {
        'n_horizon': 10,
        't_step': 0.1,
        'meas_from_data': True,
    }
    mhe.set_param(**setup_mhe)

    return mhe
