import do_mpc

def get_model():
    model_type = "continuous"
    model = do_mpc.model.Model(model_type)

    inp_1 = model.set_variable("_x", "inp_1")
    inp_2 = model.set_variable("_x", "inp_2")
    outp_1 = model.set_variable("_x", "outp_1")
    outp_2 = model.set_variable("_x", "outp_2")
    prs = model.set_variable("_x", "prs")

    inp_set_1 = model.set_variable("_u", "inp_set_1")
    inp_set_2 = model.set_variable("_u", "inp_set_2")
    outp_set_1 = model.set_variable("_u", "outp_set_1")
    outp_set_2 = model.set_variable("_u", "outp_set_2")

    prs_set_point = model.set_variable("_tvp", "prs_set_point")
    supply_1 = model.set_variable("_tvp", "supply_1")
    supply_2 = model.set_variable("_tvp", "supply_2")

    model.set_rhs("prs", ((inp_1 + inp_2) - (outp_1 + outp_2)) / 1)
    model.set_rhs("inp_1", inp_set_1 - inp_1)
    model.set_rhs("inp_2", inp_set_2 - inp_2)
    model.set_rhs("outp_1", outp_set_1 - outp_1)
    model.set_rhs("outp_2", outp_set_2 - outp_2)

    model.setup()

    return model
