import numpy as np
import do_mpc
import time
from matplotlib import pyplot as plt
import sys

from test_mpc.template_model import get_model
from test_mpc.template_mpc import get_mpc
from test_mpc.template_simulator import get_simulator
# from test_mpc.template_estimator import get_estimator

from test_mpc import plot_arrs, prs_0, n_steps


def main():
    model = get_model()
    mpc = get_mpc(model)
    simulator = get_simulator(model)
    estimator = do_mpc.estimator.StateFeedback(model)

    # ---- #

    x0 = np.array([0.0, 0.0, 0.0, 0.0, prs_0]).reshape(-1, 1)

    mpc.x0 = x0
    simulator.x0 = x0
    estimator.x0 = x0

    mpc.set_initial_guess()

    xs = [x0]
    us = []

    start_time = time.time()

    for k in range(n_steps):
        u0 = mpc.make_step(x0)
        y_next = simulator.make_step(u0)
        x0 = estimator.make_step(y_next)

        # print(f"u0: {u0.flatten()}, y_next: {y_next}, x0: {x0}")

        us.append(u0)
        xs.append(x0)

        cur_time = time.time() - start_time
        rest_time = cur_time / (k + 1) * n_steps
        print("\x1b[2K", end="\r")
        print(
            f"step: {(k+1)}/{n_steps} / current: {int(cur_time // 60)}m {round(cur_time % 60, 2)}s / estimated: {int(rest_time // 60)}m {round(rest_time % 60, 2)}s",
            end="",
        )
        sys.stdout.flush()

    total_time = time.time() - start_time
    print(f"\nTotal time: {int(total_time // 60)}m {round(total_time % 60, 2)}")

    us = np.array(us)
    xs = np.array(xs)

    plot_arrs(us, xs, "out.png")


if __name__ == "__main__":
    main()
