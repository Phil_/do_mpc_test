import do_mpc
from test_mpc import n_horizon, t_step, get_supplies, prs_wanted

def get_mpc(model):
    mpc = do_mpc.controller.MPC(model)

    setup_mpc = {
        "n_horizon": n_horizon,
        "n_robust": 1,
        "open_loop": 0,
        "t_step": t_step,
        "state_discretization": "collocation",
        "collocation_type": "radau",
        "collocation_deg": 2,
        "collocation_ni": 2,
        "store_full_solution": True,
        "nlpsol_opts": {
            "ipopt.print_level": 0,
            "ipopt.sb": "yes",
            "print_time": 0,
        },
    }

    mpc.set_param(**setup_mpc)

    # ---- #

    mterm = (model.x["prs"] - model.tvp["prs_set_point"]) ** 4
    lterm = (
        mterm
        + (model.x["inp_1"] - model.tvp["supply_1"] / 2) ** 2
        + (model.x["inp_2"] - model.tvp["supply_2"] / 2) ** 2
        + (model.x["outp_1"] - model.tvp["supply_1"] / 2) ** 2
        + (model.x["outp_2"] - model.tvp["supply_2"] / 2) ** 2
    )

    mpc.set_objective(mterm=mterm, lterm=lterm)
    mpc.set_rterm(inp_set_1=1.0, outp_set_1=1.0, inp_set_2=1.0, outp_set_2=1.0)

    # ---- #

    mpc.bounds["lower", "_x", "prs"] = -100.0
    mpc.bounds["upper", "_x", "prs"] = 100.0

    mpc.bounds["lower", "_x", "inp_1"] = 0.0
    mpc.bounds["lower", "_x", "inp_2"] = 0.0
    mpc.bounds["lower", "_x", "outp_1"] = 0.0
    mpc.bounds["lower", "_x", "outp_2"] = 0.0

    mpc.bounds["lower", "_u", "inp_set_1"] = 0.0
    mpc.bounds["lower", "_u", "inp_set_2"] = 0.0
    mpc.bounds["lower", "_u", "outp_set_1"] = 0.0
    mpc.bounds["lower", "_u", "outp_set_2"] = 0.0

    mpc.set_nl_cons(
        "sp1_cons_in", model.x["inp_1"] / (model.tvp["supply_1"] + 0.01), ub=1
    )
    mpc.set_nl_cons(
        "sp2_cons_in", model.x["inp_2"] / (model.tvp["supply_1"] + 0.01), ub=1
    )

    mpc.set_nl_cons(
        "sp1_cons_out",
        model.x["outp_1"] / (model.tvp["supply_1"] - model.x["inp_1"] + 0.01),
        ub=1,
    )
    mpc.set_nl_cons(
        "sp2_cons_out",
        model.x["outp_2"] / (model.tvp["supply_2"] - model.x["inp_2"] + 0.01),
        ub=1,
    )

    tvp_template = mpc.get_tvp_template()

    def tvp_fun(t_now):
        for k in range(n_horizon + 1):
            s1, s2 = get_supplies(t_now)
            tvp_template["_tvp", k, "prs_set_point"] = prs_wanted
            tvp_template["_tvp", k, "supply_1"] = s1
            tvp_template["_tvp", k, "supply_2"] = s2
        return tvp_template

    mpc.set_tvp_fun(tvp_fun)

    mpc.setup()

    return mpc
