import do_mpc

from test_mpc import t_step, get_supplies

def get_simulator(model):
    simulator = do_mpc.simulator.Simulator(model)

    params_simulator = {
        "integration_tool": "cvodes",
        "abstol": 1e-10,
        "reltol": 1e-10,
        "t_step": t_step,
    }

    simulator.set_param(**params_simulator)

    tvp_sim_template = simulator.get_tvp_template()

    def tvp_sim_fun(t_now):
        s1, s2 = get_supplies(t_now)
        tvp_sim_template["supply_1"] = s1
        tvp_sim_template["supply_2"] = s2
        return tvp_sim_template

    simulator.set_tvp_fun(tvp_sim_fun)

    simulator.setup()


    return simulator
