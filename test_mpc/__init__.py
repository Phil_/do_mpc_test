import matplotlib.pyplot as plt

prs_wanted = 30
n_steps = 100
t_step = 1
n_horizon = 10
prs_0 = 5


def plot_arrs(us, xs, filename="out.png"):
    plt.figure(figsize=(10, 16))

    plt.subplot(521)
    plt.title("input_set_1")
    plt.plot(us[:, 0])

    plt.subplot(522)
    plt.title("input_1")
    plt.plot(xs[:, 0])

    plt.subplot(523)
    plt.title("input_set_2")
    plt.plot(us[:, 1])

    plt.subplot(524)
    plt.title("input_2")
    plt.plot(xs[:, 1])

    plt.subplot(525)
    plt.title("output_set_1")
    plt.plot(us[:, 2])

    plt.subplot(526)
    plt.title("output_1")
    plt.plot(xs[:, 2])

    plt.subplot(527)
    plt.title("output_set_2")
    plt.plot(us[:, 3])

    plt.subplot(528)
    plt.title("output_2")
    plt.plot(xs[:, 3])

    plt.subplot(515)
    plt.title("pressure")
    plt.plot(xs[:, 4])

    plt.tight_layout()
    plt.savefig(filename)


def get_supplies(t_now):
    return (5, 2)
